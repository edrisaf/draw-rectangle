import java.util.InputMismatchException;
import java.util.Scanner;

public class DrawRectangle {

    public static int readInt(Scanner scanner) {
        // Method to read width and height, return as array
        int number = 0;
        boolean err = true;
        do {
            try {
                number = scanner.nextInt();
                if (number <= 0) {
                    throw new IllegalArgumentException();
                }
                err = false;
                // In case the input is not a positive integer, handle exception and let user
                // try
                // again
            } catch (InputMismatchException e) {
                System.out.println("Width/height must be positive integers. Try again: ");
                scanner.next();
            } catch (IllegalArgumentException e) {
                System.out.println("Width/height must be positive integers. Try again: ");
            }
        } while (err);
        return number;
    }

    public static void drawRect(int w, int h, char t) {
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                // Check if it is the first or last row/col and draw
                if (i == 0 || i == h - 1 || j == 0 || j == w - 1) {
                    System.out.print(t);
                } else {
                    System.out.print(" ");
                }
            }
            // Start a new line when current row is drawn
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        do {
            System.out.print("Enter rectangle width: ");
            int width = readInt(scan);

            System.out.print("Enter rectangle height: ");
            int height = readInt(scan);

            System.out.print("Enter the character: ");
            char token = scan.next().charAt(0);
            drawRect(width, height, token);

            System.out.print("Enter 'q' to quit, anything else to continue: ");
        } while (scan.next().compareToIgnoreCase("q") != 0);
        scan.close();
    }
}
